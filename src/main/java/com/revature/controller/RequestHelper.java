package com.revature.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.model.User;
import com.revature.service.BankService;
import com.revature.service.StudentService;

/*
 * This class is a helper class for our Front Controller. Our Front Controller will
 * delegate to this request helper. I've chosen to use this request helper because
 * I would like to move some of the logic needed for processing requests here as it
 * is not needed within our Front Controller.
 */
public class RequestHelper {

	/*
	 * Prior to today, we used the System class to print to stdout. That said, this
	 * is not appropriate in production (especially if you're building an
	 * application where the console is visible). Further still, once the
	 * application stops running, we lose all of the information that was written to
	 * the console.
	 * 
	 * As such, we are now going to transition to using Loggers instead. Our
	 * implementation for logging is Log4J.
	 */

	private static final Logger LOG = LogManager.getLogger(RequestHelper.class);

	// So we define helper methods here such as...

	// A helper method which helps me process GET requests. When I say
	// "process" GET requests, I mean that I would like to find out which resource
	// is being requested as our Front Controller uses a wildcard symbol; this
	// requires
	// that we parse the request URL to find out which resource was requested.

	public static Object processGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		/*
		 * Remember: We need to parse the request URL. Fortunately, we can use the
		 * request object to access the URL.
		 */
		LOG.debug("The request URI is: " + request.getRequestURI());

		/*
		 * We are isolating the final piece of the URI to determine the exact resource
		 * which has been requested by the client.
		 */
		final String URI = request.getRequestURI();
		String resource = URI.replace("/BankAPIServlets-0.0.1-SNAPSHOT/myapi", "");

		LOG.debug("The resource is requested is: " + resource);
		System.out.println("The resource is requested is: " + resource);

		switch (resource) {
		case "/student/all":
			return new StudentService().findAll();
		case "/users":
			return new BankService().findAll();
		case "/insertUser":
			return insertUser(request, response);

		default:
			return "No such resource exists on this api";
		}

	}

	private static Object insertUser(HttpServletRequest request, HttpServletResponse response) {
		String message="";
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream()));
			System.out.println("came here to insert user");
			String json = "";
			if (br != null) {
				json = br.readLine();
				System.out.println(json);
				ObjectMapper mapper = new ObjectMapper();
				User user = mapper.readValue(json, User.class);
				response.setContentType("application/json");
				System.out.println("The user to be inserted:"+user);
				int result= new BankService().insert(user);
				if (result==1) {
					message="User insert successful";
				}else 
					message="User insert NOT successful";
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return message;
	}
}
