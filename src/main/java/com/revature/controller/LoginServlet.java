package com.revature.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.revature.model.User;
import com.revature.service.BankService;

/**
 * Servlet implementation class LoginServlet
 */
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/*
		 * Note that I am putting my logic for POST requests here since doPost calls
		 * doGet anyway.
		 * 
		 * In this scenario, we must imagine that a password and username are required
		 * in order to access resources on our server.
		 */

		final String username = request.getParameter("username");
		System.out.println("username supplied:" + username);
		/*
		 * We've grabbed data sent from the client. Now we should use this data to
		 * attempt to authenticate the user. For us, this entails checking that the user
		 * exists in our database.
		 */

		BankService bankService = new BankService();
		User user = bankService.findByName(username);
		System.out.println("user found:" + user);

		if (user != null) {
			// But does the amount of financial aid match? We can check!
			/*
			 * Note that Wrapper classes for our primitives are very convenient for parsing
			 * Strings as numeric values.
			 */

			HttpSession session = request.getSession(); // this creates a session or retrieves an existing one
			session.setAttribute("user", user.getUserId());

			/*
			 * Sometimes we want to immediately redirect a client to a different resource
			 * from another.
			 * 
			 * COMMENTED OUT FOR DEMO PURPOSES.
			 */

//				response.sendRedirect("/StudyAppServlets/myapi/student/all");

			/*
			 * We can also simply forward the request to another resource on the server. We
			 * need a RequestDispatcher for this.
			 */

			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/users");
			requestDispatcher.forward(request, response);

		}else
		{
			String jsonStr = "No user credential Found";
			response.getWriter().write(jsonStr);

		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
