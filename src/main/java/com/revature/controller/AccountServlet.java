package com.revature.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.model.User;
import com.revature.service.BankService;

/**
 * Servlet implementation class LoginServlet
 */
public class AccountServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger LOG = LogManager.getLogger(AccountServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AccountServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {


		BankService bankService = new BankService();
		
		List<User> userList = bankService.findAll();
		LOG.info("userlist"+userList);
		System.out.println("userlist"+userList);
		
		ObjectMapper mapper = new ObjectMapper();
		String jsonStr = "No user Found";
		
		
		if (userList != null && userList.size()>0) {
			jsonStr="";
			HttpSession session = request.getSession(); // this creates a session or retrieves an existing one
			session.setAttribute("user", userList.get(0).getUserId());

			LOG.info("going to build string for JSON" + userList.size());
			for (int i = 0; i < userList.size(); i++) {
				jsonStr = jsonStr+mapper.writeValueAsString(userList.get(i));
			}
		}
		response.getWriter().write(jsonStr);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
