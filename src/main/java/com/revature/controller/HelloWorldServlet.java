package com.revature.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.model.Student;

/*
 * This class is officially a servlet class once we've extended the HttpServlet class.
 */

public class HelloWorldServlet extends HttpServlet{
	
	/*
	 * Recall that the web container is responsible for managing the life cycle of a
	 * servlet. This means that the web container decides when an instance of a servlet
	 * class is needed and when it is no longer needed. It is in charge of the servlets'
	 * life cycle. The life cycle consists of three method calls:
	 * 
	 * init() -> called only once
	 * service() -> called many times
	 * destroy() -> called only once
	 * 
	 * These methods are inherited, so we don't have to define these. Note that I've
	 * placed them below for your reference.
	 */
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void init() throws ServletException {
		// TODO Auto-generated method stub
		super.init();
	}
	
	@Override
	public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
		// TODO Auto-generated method stub
		super.service(req, res);
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		super.destroy();
	}
	

	/*
	 * Let's create a simple method which can handle GET requests. This handler method
	 * will handle GET requests and no other types of requests (e.g. POST, PUT, etc.).
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		
		response.setStatus(200); //you can set the status code
		
//		SENDING HTML TO THE CLIENT
//		response.setContentType("text/html");
//		response.getWriter().write("<h1>This is HTML.</h1><p>This is a paragraph</p>");
		
//		SENDING JSON TO THE CLIENT
		/*
		 * We are NOT formatting our own JSON as this looks really gross. Instead, we'll
		 * use a convenient class called the ObjectMapper class. This comes from our
		 * Jackson databind dependency we included in our pom.xml.
		 */
		ObjectMapper imTheMap = new ObjectMapper();
		final String JSON = imTheMap.writeValueAsString(new Student(1, "Deku", 2342.44f, 1));
		response.setContentType("application/json");
		response.getWriter().write(JSON);
		
//		METHOD CHAINING: Not as clean or easy to read in this case.
//		response.getWriter()
//		.write(new ObjectMapper().
//				writeValueAsString(new Student(1, "Deku", 234234.33f, "Heroism")));
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		
		/*
		 * Above, we've seen that we can use our response object to populate our HTTP response.
		 * That said, we can use the request object to view information about the HTTP
		 * request. We'll use our request object to grab request parameters send to the
		 * server from the client.
		 * 
		 * Do note that you always want to perform some validation on the server side
		 * for any inputs taken from the client side. Sometimes people, for instance,
		 * with malicious intent modify the HTML document in their client and forgo
		 * sending back required information.
		 */
		
		final String username = request.getParameter("username");
		final String pass = request.getParameter("password");
		final String address = request.getParameter("address");
		final String nonexistent = request.getParameter("usernam");
			
//		response.getWriter().write(username + " " + pass + " " + address + " " + nonexistent);
		
		/*
		 * For those who want to retrieve session attributes:
		 */
		
		final String user = (String) request.getSession().getAttribute("user");
		
		response.getWriter().write("Welcome back, " + user + "!");
	}
}
