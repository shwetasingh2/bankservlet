package com.revature.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.revature.model.Student;
import com.revature.service.StudentService;

/**
 * Servlet implementation class LoginServlet
 */
public class LoginServlet_bkup extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet_bkup() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/*
		 * Note that I am putting my logic for POST requests here since doPost
		 * calls doGet anyway.
		 * 
		 * In this scenario, we must imagine that a password and username are
		 * required in order to access resources on our server.
		 */
		
		final String username = request.getParameter("username");
		final String finAid = request.getParameter("finAid");
		
		/*
		 * We've grabbed data sent from the client. Now we should use this data to
		 * attempt to authenticate the user. For us, this entails checking that the
		 * user exists in our database.
		 */
		
		StudentService studentService = new StudentService();
		Student student = studentService.findByName(username);
//		StudentService studentService = new StudentService();
//		Student student = studentService.findByName(username);
		
		if(student != null) {
			//But does the amount of financial aid match? We can check!
			/*
			 * Note that Wrapper classes for our primitives are very convenient for
			 * parsing Strings as numeric values.
			 */
			float convertedFinAid = Float.parseFloat(finAid);
			
			if(convertedFinAid == student.getFinancialAid()) {
				HttpSession session = request.getSession(); //this creates a session or retrieves an existing one
				/*
				 * Optional (but can be useful). You can use what we refer to as
				 * "session attributes". Session attributes are key value pairs.
				 * You decide what the key and value are. They are typically information
				 * that would be useful to cache on the server.
				 * 
				 * This information is discarded once your session has been
				 * invalidated!
				 */
				
				session.setAttribute("user", student.getName());
				
				/*
				 * Sometimes we want to immediately redirect a client to a different
				 * resource from another.
				 * 
				 * COMMENTED OUT FOR DEMO PURPOSES. 
				 */
				
//				response.sendRedirect("/StudyAppServlets/myapi/student/all");
				
				/*
				 * We can also simply forward the request to another resource on the
				 * server. We need a RequestDispatcher for this.
				 */
				
				RequestDispatcher requestDispatcher = 
						request.getRequestDispatcher("/myapi/student/all");
				requestDispatcher.forward(request, response);
				
				
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
