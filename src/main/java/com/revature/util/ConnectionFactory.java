package com.revature.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/*
 * This is a utility class that will be used to return JDBC connections
 * as needed. Using this abstraction, we can write the code that handles
 * passing in our credentials a single time.
 */
public class ConnectionFactory {

	private static Connection conn;
	
	public static Connection getConnection() {
		try {
			/*
			 * If your Driver class cannot be located, you can force Java to load
			 * the class like so:
			 */
			System.out.println("came to get the connection");
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			System.out.println("loaded the class");

//			conn = DriverManager.getConnection(
//					System.getenv("url"),
//					System.getenv("postgres_username"),
//					System.getenv("postgres_password")
//		);			
			
			System.out.println("connection is --" + conn);
			conn = DriverManager.getConnection(
				"jdbc:sqlserver://localhost\\MSSQLSERVER:1433;databaseName=bankapi",
				"batch_user",
				"123456"
	);
			System.out.println("connection is 2" + conn);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return conn;
				
	}
}
