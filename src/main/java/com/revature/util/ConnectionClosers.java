package com.revature.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectionClosers {

	public static void closeConnection(Connection conn) {
		try {
			if (conn!=null)
				conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static void closeStatement(Statement stmt) {
		try {
			if(stmt !=null)
				stmt.close();
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static void closeResultSet(ResultSet set) {
		try {
			if(set !=null)
				set.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
