package com.revature.repository;

import java.util.List;

import com.revature.model.User;

public interface BankAPIRepository {

	List<User> findAll();
	User findById(int id);
	User findByName(String name);
	int insert(User user);
	int delete(User user	);
	int update(User user);
}
