package com.revature.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.revature.model.Role;
import com.revature.model.User;
import com.revature.util.ConnectionClosers;
import com.revature.util.ConnectionFactory;

public class BankAPIRepositoryImpl implements BankAPIRepository {
	private static final Logger LOG = LogManager.getLogger(BankAPIRepositoryImpl.class);

	/*
	 * Let's find all students within the database.
	 */
	@Override
	public List<User> findAll() {
		/*
		 * Remember that this method should return a List of students. So let's define
		 * one here:
		 */
		ArrayList<User> users = new ArrayList<>();

		/*
		 * The first thing we always need to do is establish a connection to our
		 * database by providing our credentials.
		 */
		Connection conn = null;
		/*
		 * After we have established a connection to the DB, we can execute SQL
		 * statements.
		 */
		Statement stmt = null;
		/*
		 * After we've executed our statement, we want to store and access the result
		 * set.
		 */
		ResultSet rs = null;
		try {
			System.out.println("Came to pull all users");
			conn = ConnectionFactory.getConnection();
			stmt = conn.createStatement();
			rs = stmt
					.executeQuery("select [userId],[username],[firstName], [lastName] ,[email] ,[roleid] from allUser");
			System.out.println("resultSet" + rs);
			LOG.info("resultSet" + rs);

			/*
			 * We can move through each record in a ResultSet and extract the data!
			 */
			while (rs.next()) {
				System.out.println("iterating resultSet...");
				LOG.info("iterating resultSet...");

				User user = new User();
				user.setUserId(rs.getInt(1));
				user.setUsername(rs.getString(2));
				user.setFirstName(rs.getString(3));
				user.setLastName(rs.getString(4));
				user.setEmail(rs.getString(5));
				user.setRole(new Role(rs.getInt(6), ""));
				users.add(user);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			/*
			 * No matter what, always close your database connections!
			 */
			try {
				stmt.close();
				rs.close();
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return users;
	}

	/*
	 * This method will find a student by their id (which is the primary key). This
	 * means that this method is guaranteed to return a single student.
	 */
	@Override
	public User findById(int id) {
		/*
		 * We need to return a Student!
		 */
		User user = null;
		Connection conn = null;
		Statement stmt = null;
		ResultSet set = null;
		final String SQL = "select * from user where userid = " + id;

		try {
			conn = ConnectionFactory.getConnection();
			stmt = conn.createStatement();
			set = stmt.executeQuery(SQL);

			/*
			 * We know that the ResultSet only has one record, so we only need to move the
			 * cursor once!
			 * 
			 * As an addendum, the above strategy only works if you are 100% sure that the
			 * student id exists. That said, do check using the while loop!
			 */
			while (set.next()) {
				user = new User();
				user.setUserId(set.getInt(1));
				user.setUsername(set.getString(2));
				user.setFirstName(set.getString(3));
				user.setLastName(set.getString(4));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionClosers.closeConnection(conn);
			ConnectionClosers.closeStatement(stmt);
			ConnectionClosers.closeResultSet(set);
		}
		return user;
	}

	@Override
	public User findByName(String name) {
		User user = null;
		System.out.println("Repo...name is:" + name);
		LOG.info("Repo...name is:" + name);
		Connection conn = null;
		/*
		 * We are taking user input and passing it into our SQL String. That said, there
		 * is some risk of SQL injection. As such, we need to use a PreparedStatement to
		 * protect against that SQL injection.
		 */
		PreparedStatement stmt = null;
		ResultSet set = null;
		final String SQL = "select * from alluser where [username] = ? ";

		try {
			conn = ConnectionFactory.getConnection();
			stmt = conn.prepareStatement(SQL);
			/*
			 * We have called "prepareStatement". This does not execute your query. Note
			 * that PreparedStatements work by precompiling your query so that you can
			 * parameterize the "input" passed in by your user (or from any other source).
			 */

			stmt.setString(1, name );

			set = stmt.executeQuery();
			System.out.println("Repo...result set it " + set);
			LOG.info("Repo...result set it " + set);

			while (set.next()) {
				user = new User();
				user.setUserId(set.getInt(1));
				user.setUsername(set.getString(2));
				user.setFirstName(set.getString(3));
				user.setLastName(set.getString(4));

			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionClosers.closeConnection(conn);
			ConnectionClosers.closeResultSet(set);
			ConnectionClosers.closeStatement(stmt);
		}

		System.out.println("Returning user:" + user);
		LOG.info("Returning user:" + user);
		return user;
	}

	@Override
	public int insert(User user) {
		Connection conn = null;
		PreparedStatement stmt = null;
		int result=0;
		/*
		 * Note that if your primary key is serial, you can just place "default" in the
		 * place of the first question mark.
		 */
		final String SQL = "INSERT INTO [dbo].[allUser] "
				+ "([userId],[username],[password],[firstName],[lastName],[email],[roleid]) VALUES "
				+ "( ? , ? , ? , ? , ? , ? , ?)";

		try {
			conn = ConnectionFactory.getConnection();
			stmt = conn.prepareStatement(SQL);
			stmt.setInt(1, user.getUserId());
			stmt.setString(2, user.getUsername());
			stmt.setString(3, user.getPassword());
			stmt.setString(4, user.getFirstName());
			stmt.setString(5, user.getLastName());
			stmt.setString(6, user.getEmail());
			stmt.setInt(7, user.getRole().getRoleId());
			stmt.execute();
			result=1;
		} catch (SQLException e) {
			e.printStackTrace();
			result=0;
		} finally {
			ConnectionClosers.closeConnection(conn);
			ConnectionClosers.closeStatement(stmt);
		}

		System.out.println("result is:"+result);
		return result;
	}

	@Override
	public int delete(User user) {
		Connection conn = null;
		PreparedStatement stmt = null;
		final String deleteSql = "delete [dbo].[allUser] where userid=?";
		int result=0;

		try {
			conn = ConnectionFactory.getConnection();
			stmt = conn.prepareStatement(deleteSql);
			stmt.setInt(1, user.getUserId());

			stmt.execute();
			result=1;

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionClosers.closeConnection(conn);
			ConnectionClosers.closeStatement(stmt);
		}
		return result;
	}

	@Override
	public int update(User user) {
		int result=0;
		return result;
	}

}
