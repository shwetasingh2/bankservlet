package com.revature.repository;

import java.util.List;

import com.revature.model.Student;

public interface StudentRepository {

	List<Student> findAll();
	Student findById(int id);
	Student findByName(String name);
	void insert(Student student);
	void delete(Student student);
	void update(Student student);
}
