package com.revature.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.revature.model.Student;
import com.revature.util.ConnectionClosers;
import com.revature.util.ConnectionFactory;

public class StudentRepositoryImpl implements StudentRepository {

	/*
	 * Let's find all students within the database.
	 */
	@Override
	public List<Student> findAll() {
		/*
		 * Remember that this method should return a List of students. So let's define
		 * one here:
		 */
		ArrayList<Student> students = new ArrayList<>();

		/*
		 * The first thing we always need to do is establish a connection to our
		 * database by providing our credentials.
		 */
		Connection conn = null;
		/*
		 * After we have established a connection to the DB, we can execute SQL
		 * statements.
		 */
		Statement stmt = null;
		/*
		 * After we've executed our statement, we want to store and access the result
		 * set.
		 */
		ResultSet set = null;
		try {
			conn = ConnectionFactory.getConnection();
			stmt = conn.createStatement();
			set = stmt.executeQuery("select * from student");

			/*
			 * We can move through each record in a ResultSet and extract the data!
			 */
			while (set.next()) {
				students.add(new Student(set.getInt(1), set.getString(2), set.getFloat(3), set.getInt(4)));
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			/*
			 * No matter what, always close your database connections!
			 */
			try {
				stmt.close();
				set.close();
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return students;
	}

	/*
	 * This method will find a student by their id (which is the primary key). This
	 * means that this method is guaranteed to return a single student.
	 */
	@Override
	public Student findById(int id) {
		/*
		 * We need to return a Student!
		 */
		Student student = null;
		Connection conn = null;
		Statement stmt = null;
		ResultSet set = null;
		final String SQL = "select * from student where id = " + id;

		try {
			conn = ConnectionFactory.getConnection();
			stmt = conn.createStatement();
			set = stmt.executeQuery(SQL);

			/*
			 * We know that the ResultSet only has one record, so we only need to move the
			 * cursor once!
			 * 
			 * As an addendum, the above strategy only works if you are 100% sure that the
			 * student id exists. That said, do check using the while loop!
			 */
			while (set.next()) {
				student = new Student(set.getInt(1), set.getString(2), set.getFloat(3), set.getInt(4));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionClosers.closeConnection(conn);
			ConnectionClosers.closeStatement(stmt);
			ConnectionClosers.closeResultSet(set);
		}
		return student;
	}

	@Override
	public Student findByName(String name) {
		Student student = null;
		
		Connection conn = null;
		/*
		 * We are taking user input and passing it into our SQL String. That
		 * said, there is some risk of SQL injection. As such, we need to use
		 * a PreparedStatement to protect against that SQL injection. 
		 */
		PreparedStatement stmt = null;
		ResultSet set = null;
		final String SQL = "select * from student where student_name = ?";
		
		try {
			conn = ConnectionFactory.getConnection();
			stmt = conn.prepareStatement(SQL);
			/*
			 * We have called "prepareStatement". This does not execute your
			 * query. Note that PreparedStatements work by precompiling your 
			 * query so that you can parameterize the "input" passed in by
			 * your user (or from any other source).
			 */
			stmt.setString(1, name);
			set = stmt.executeQuery();
			
			while(set.next()) {
				student = new Student(set.getInt(1),
						set.getString(2),
						set.getFloat(3),
						set.getInt(4));
			}
			
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			ConnectionClosers.closeConnection(conn);
			ConnectionClosers.closeResultSet(set);
			ConnectionClosers.closeStatement(stmt);
		}
		
		return student;
	}

	@Override
	public void insert(Student student) {
		Connection conn = null;
		PreparedStatement stmt = null;
		/*
		 * Note that if your primary key is serial, you can just place "default"
		 * in the place of the first question mark.
		 */
		final String SQL = "insert into student values(?, ?, ?, ?)";
		
		try {
			conn = ConnectionFactory.getConnection();
			stmt = conn.prepareStatement(SQL);
			stmt.setInt(1, student.getId());
			stmt.setString(2, student.getName());
			stmt.setFloat(3, student.getFinancialAid());
			stmt.setInt(4, student.getFieldOfStudy());
			stmt.execute();
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			ConnectionClosers.closeConnection(conn);
			ConnectionClosers.closeStatement(stmt);
		}
		
	}

	@Override
	public void delete(Student student) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(Student student) {
		// TODO Auto-generated method stub
		
	}

}
