package com.revature.model;

/*
 * All classes inherit from the Object class. As such, they all have access
 * to any methods defined on the Object class. Some of those include:
 * 
 * toString()
 * equals()
 * hashCode()
 * 
 * NOTE: This class follows a well-known design pattern referred to as the 
 * Java Bean design pattern. A Java Bean has:
 * 
 * getters and setters
 * a constructor using fields
 * a no-args constructor
 * a hashCode, equals, and toString method
 * and (technically should) implement the Serializable interface
 * 
 * OPTIONAL NOTE: If you want to define a natural order for this model, 
 * it should implement the Comparable interface and override the compareTo
 * method!
 */
public class Student implements Comparable<Student> {

	/*
	 * We're creating properties (known as fields) of our Student class. Each
	 * instance of Student will have its own copy of these fields.
	 * 
	 * Why are we using "public" and "private" with these fields? These are known as
	 * access modifiers, and they determine where the fields can be DIRECTLY
	 * accessed (meaning we can use the reference to access them directly).
	 * 
	 * A field (or method!) that is public can be accessed anywhere.
	 * 
	 * A protected field can only be directly accessed within the class it is
	 * declared, within the same package, and within child classes of the class in
	 * which it declared.
	 * 
	 * A default field (which is NOT declared using the "default" keyword) can only
	 * be directly accessed within the same package.
	 * 
	 * A private field can only be directly accessed within the class in which it is
	 * declared.
	 * 
	 * NOTE: Access modifiers were modified to properly encapsulate this class. In
	 * order to access these fields outside of this class, we have created getters
	 * and setters.
	 */
	protected int id;
	protected String name;
	protected float financialAid;
	protected int fieldOfStudy;
	/*
	 * Let's hone in on the "static" keyword. This keyword can be used with fields,
	 * methods, and even inner classes!
	 * 
	 * But what does it mean for these things to be static?
	 * 
	 * Simply put, a static member of a class belongs to the class rather than to
	 * any one instance of the class. In other words, there is only one copy of a
	 * static member. All instances of this class share this single instance of
	 * numberOfStudents.
	 * 
	 * We will use this static member as a counter to keep track of the number of
	 * instances of the Student class we create.
	 * 
	 * Note that you cannot access non-static members within a static context.
	 */

	public static int numberOfStudents;

	/*
	 * In Java, a default constructor is provided if we do not create our own
	 * constructor. The moment you specify a constructor, you are no longer using
	 * Java's default constructor.
	 */
	public Student() {
		/*
		 * Even if you don't write this, the first line of any constructor is a call to
		 * the parent class' constructor!
		 */
		super();
		/*
		 * This is what we refer to as a no-args constructor; we have passed no
		 * arguments to it!
		 */

		/*
		 * Usually, if you have a no-args constructor, you can at least initialize your
		 * fields to some default values.
		 */
		this.id = 0;
		this.name = "n/a";
		this.financialAid = 0.0f;
		this.fieldOfStudy = 0;

		numberOfStudents++;
	}

	public Student(int id, String name, float financialAid, int fieldOfStudy) {
		this.id = id;
		this.name = name;
		this.financialAid = financialAid;
		this.fieldOfStudy = fieldOfStudy;

		numberOfStudents++;
	}

	// GETTERS AND SETTERS

	public int getId() {
		return id;
	}

	public void setId(int id) {
		/*
		 * If we allowed direct access to a field, we couldn't protect against bad input
		 * using logic.
		 */
		if (id < 1) {
			this.id = 0;
		}
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getFinancialAid() {
		return financialAid;
	}

	/*
	 * Let's throw an Exception whenever a negative number is passed in for the
	 * amount of financial aid!
	 * 
	 * We've now modified this method so that the caller of the method will have to
	 * handle the exception. We've done so by using the "throws" keyword to denote
	 * that this method possibly throws an exception.
	 */
	public void setFinancialAid(float financialAid) {

		this.financialAid = financialAid;
	}

	public int getFieldOfStudy() {
		return fieldOfStudy;
	}

	public void setFieldOfStudy(int fieldOfStudy) {
		this.fieldOfStudy = fieldOfStudy;
	}

	/*
	 * We can also add behavior to our students by defining what we refer to as
	 * "methods." Methods are blocks of code which define a sequence of statements;
	 * note that we can reuse methods as many times as we want to!
	 */

	/*
	 * Students do homework! So our method for modeling this behavior will have:
	 * 
	 * An access modifier A return type (any valid data type or void if nothing is
	 * returned) Optional non-access modifiers A method name Optional parameters
	 */
	public void doHomework() {
		System.out.println(name + " is doing homework!");
	}

	// Students also think frantically about the amount of things they have to do
	public String countTasks(String task) {
		/*
		 * When we work with arrays or collections, let's refactor this!
		 */
		return this.name + " has " + task + " on their schedule.";
	}

	public String countTasks(String task1, String task2) {
		return this.name + " has " + task1 + " and " + task2 + " on their schedule.";
	}

	/*
	 * What if there was some way to create a method which can account for any
	 * number of tasks? In fact, there is: variable arguments (var-args).
	 * 
	 * Note: There are some limitations on the arguments.
	 * 
	 * 1) You can only have one variable argument in your list of parameters. 2)
	 * Your variable argument must be the last argument.
	 */

	public String countTasks(String... tasks) {

		String resultingSentence = this.name + " has ";

		// enhanced for loop
		for (String task : tasks) {
			resultingSentence = resultingSentence.concat(task + ", ");
		}

		resultingSentence = resultingSentence.concat("on their schedule today.");

		return resultingSentence;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + fieldOfStudy;
		result = prime * result + Float.floatToIntBits(financialAid);
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		if (fieldOfStudy != other.fieldOfStudy)
			return false;
		if (Float.floatToIntBits(financialAid) != Float.floatToIntBits(other.financialAid))
			return false;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", financialAid=" + financialAid + ", fieldOfStudy="
				+ fieldOfStudy + "]";
	}

	// OPTIONAL: Method to be overridden when using Comparable interface
	@Override
	public int compareTo(Student o) {
		if (this.name.equals(o.name))
			return 0;
		else if (this.name.compareTo(o.name) > 0)
			return 1;
		else
			return -1;
	}

}
