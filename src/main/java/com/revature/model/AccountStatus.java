package com.revature.model;

public class AccountStatus {

	public AccountStatus() {
		// TODO Auto-generated constructor stub
	}

	private int statusId; // primary key
	private String status; // not null, unique

	
	
	/* All getter and setter method defined here */

	public AccountStatus(int statusId, String status) {
		super();
		this.statusId = statusId;
		this.status = status;
	}

	public int getStatusId() {
		return statusId;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
