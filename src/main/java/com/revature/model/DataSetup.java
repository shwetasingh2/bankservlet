package com.revature.model;

import java.util.ArrayList;
import java.util.List;

public class DataSetup {

	private List<User> userList = new ArrayList<User>();
	private List<Account> accountList = new ArrayList<Account>();
	

	public DataSetup() {
		
		/* Setup AccountType */	
		AccountType savingsAccount = new AccountType(100, "Savings");
		AccountType checkingAccount = new AccountType(200, "Checking");

		/* Setup AccountStatus */
		AccountStatus pendingStatus = new AccountStatus(100, "Pending");
		AccountStatus openStatus = new AccountStatus(200, "Open");
		AccountStatus closedStatus = new AccountStatus(300, "Closed");
		AccountStatus deniedStatus = new AccountStatus(400, "Denied");

		/* Setup AccountRole */
		Role adminRole = new Role(100, "Admin");
		Role employeeRole = new Role(200, "Employee");
		Role standardRole = new Role(100, "Standard");
		Role premiumRole = new Role(100, "Premium");

		/* create user */
		User user1 = new User(100, "Michael", "****", "Michael", "Test", "email1", adminRole);
		User user1Employee = new User(200, "Buton", "****", "Button", "Cheny", "Button.Cheny@gmail.com",	employeeRole);
		User user1Standard = new User(300, "Anvika", "****", "Anvika", "Singh", "anvika.singh@gmail.com",	standardRole);
		User user1PremiumRole = new User(400, "testUser", "****", "test", "user", "test.user@gmail.com",	premiumRole);

		User user2 = new User(500, "adminUser", "****", "firstname2", "lastName2", "email2", adminRole);
		User user2Employee = new User(600, "Scott", "****", "firstname2", "lastName2", "email2",employeeRole);
		User user2Standard = new User(700, "Tiger", "****", "firstname2", "lastName2", "email2",standardRole);
		User user2PremiumRole = new User(800, "TestUser2", "****", "firstname2", "lastName2", "email2",premiumRole);
		userList.add(user1);
		userList.add(user1Employee);
		userList.add(user1Standard);
		userList.add(user1PremiumRole);
		
		userList.add(user2);
		userList.add(user2Employee);
		userList.add(user2Standard);
		userList.add(user2PremiumRole);
		
		/* create Account */
		Account account1 = new Account(100, 100.00, pendingStatus, checkingAccount);
		Account account2 = new Account(200, 200.00, openStatus, checkingAccount);
		Account account3 = new Account(300, 300.00, closedStatus, savingsAccount);
		Account account4 = new Account(400, 400.00, openStatus, checkingAccount);
		Account account5 = new Account(500, 500.00, openStatus, checkingAccount);
		Account account6 = new Account(600, 600.00, openStatus, checkingAccount);
		Account account7 = new Account(700, 700.00, openStatus, checkingAccount);
		Account account8 = new Account(800, 800.00, openStatus, savingsAccount);
		Account account9 = new Account(900, 900.00, openStatus, savingsAccount);
		Account account10 = new Account(1000, 1000.00, openStatus, savingsAccount);
		Account account11 = new Account(1100, 1100.00, openStatus, savingsAccount);

		Account account100 = new Account(90000000, 0.00, deniedStatus, savingsAccount);
		Account account101 = new Account(90000000, 0.00, deniedStatus, checkingAccount);
		


		
		
		accountList.add(account1);
		accountList.add(account2);
		accountList.add(account3);
		accountList.add(account4);
		accountList.add(account5);
		accountList.add(account6);
		accountList.add(account7);
		accountList.add(account8);
		accountList.add(account9);
		accountList.add(account10);
		accountList.add(account11);
		accountList.add(account100);
		accountList.add(account101);
	}


	public List<User> getUserList() {
		return userList;
	}

	public void setUserList(List<User> userList) {
		this.userList = userList;
	}
	
	
	public List<Account> getAccountList() {
		return accountList;
	}

	public void setaccountList(List<Account> accountList) {
		this.accountList = accountList;
	}
}
