package com.revature.model;

public class AccountType {

	public AccountType() {
		// TODO Auto-generated constructor stub
	}



	public AccountType(int typeId, String type) {
		super();
		this.typeId = typeId;
		this.type = type;
	}



	private int typeId; // primary key
	private String type; // not null, unique

	/* All getter and setter method defined here */
	public int getTypeId() {
		return typeId;
	}

	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
