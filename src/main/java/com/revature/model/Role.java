package com.revature.model;


public class Role {

	public Role() {
		// TODO Auto-generated constructor stub
	}
	private int roleId; // primary key
	
	private String role; // not null, unique
	
	
	public Role(int roleId, String role) {
		super();
		this.roleId = roleId;
		this.role = role;
	}

	/* All getter and setter method defined here */

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

}
