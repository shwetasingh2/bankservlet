package com.revature.model;

public class Account {

	public Account() {
		// TODO Auto-generated constructor stub
	}

	private int accountId; // primary key
	private double balance; // not null
	private AccountStatus accountStatus;
	private AccountType accountType;

	/* All getter and setter method defined here */

	public Account(int accountId, double balance, AccountStatus status, AccountType type) {
		super();
		this.accountId = accountId;
		this.balance = balance;
		this.accountStatus = status;
		this.accountType = type;
	}

	public int getAccountId() {
		return accountId;
	}

	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public AccountStatus getAccountStatus() {
		return accountStatus;
	}

	public void setAccountStatus(AccountStatus accountStatus) {
		this.accountStatus = accountStatus;
	}

	public AccountType getAccountType() {
		return accountType;
	}

	public void setAccountType(AccountType type) {
		this.accountType = type;
	}

}
