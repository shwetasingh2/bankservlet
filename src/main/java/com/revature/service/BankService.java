package com.revature.service;

import java.util.List;

import com.revature.model.User;
import com.revature.repository.BankAPIRepository;
import com.revature.repository.BankAPIRepositoryImpl;

/*
 * This layer of the application is dedicated to business logic!
 */
public class BankService {

	private BankAPIRepository bankapiRepository;
	
	public BankService() {
		this.bankapiRepository = new BankAPIRepositoryImpl();
	}
	
	public List<User> findAll(){
		return this.bankapiRepository.findAll();
	}
	
	public User findById(int id) {
		return this.bankapiRepository.findById(id);
	}
	
	public User findByName(String name) {
		return this.bankapiRepository.findByName(name);
	}
	
	public int insert(User user) {
		return this.bankapiRepository.insert(user);
	}
	public int delete(User user) {
		return this.bankapiRepository.delete(user);
	}
	
	/* Account Related API */ 
	
	
}
