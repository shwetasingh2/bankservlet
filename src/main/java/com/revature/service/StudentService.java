package com.revature.service;

import java.util.List;

import com.revature.model.Student;
import com.revature.repository.StudentRepository;
import com.revature.repository.StudentRepositoryImpl;

/*
 * This layer of the application is dedicated to business logic!
 */
public class StudentService {

	private StudentRepository studentRepository;
	
	public StudentService() {
		this.studentRepository = new StudentRepositoryImpl();
	}
	
	public List<Student> findAll(){
		return this.studentRepository.findAll();
	}
	
	public Student findById(int id) {
		return this.studentRepository.findById(id);
	}
	
	public Student findByName(String name) {
		return this.studentRepository.findByName(name);
	}
	
	public void insert(Student student) {
		this.studentRepository.insert(student);
	}
}
