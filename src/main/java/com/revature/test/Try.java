package com.revature.test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.revature.model.Role;
import com.revature.model.User;
import com.revature.util.ConnectionClosers;
import com.revature.util.ConnectionFactory;

public class Try {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Connection conn = null; 
		Role role = null;
		Statement stmt = null;
		ResultSet set = null;
		final String SQL = "select * from bankapi.dbo.userRole ";
		System.out.println("came here111 " + conn);

		try {
			conn = ConnectionFactory.getConnection();
			stmt = conn.createStatement();
			set = stmt.executeQuery(SQL);

			/*
			 * We know that the ResultSet only has one record, so we only need to move the
			 * cursor once!
			 * 
			 * As an addendum, the above strategy only works if you are 100% sure that the
			 * student id exists. That said, do check using the while loop!
			 */
			System.out.println("came here " + conn);

			while (set.next()) {
				System.out.println("row 1" + set.getInt(1));
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		} 
		
		finally {
			ConnectionClosers.closeResultSet(set);
			ConnectionClosers.closeStatement(stmt);
			ConnectionClosers.closeConnection(conn);
		}
		
	}

}
