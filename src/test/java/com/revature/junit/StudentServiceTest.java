package com.revature.junit;

import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.revature.model.Student;
import com.revature.repository.StudentRepositoryImpl;
import com.revature.service.StudentService;

/*
 * JUnit is a testing framework for Java that is considered easy and simple
 * to use. It is VERY popular, so many IDEs already have built-in support for
 * writing and running JUnit tests.
 * 
 * JUnit is annotation-driven, so we'll use MANY annotations today.
 * 
 * We're now adding Mockito to this test suite in order to perform true unit tests.
 * Note that Mockito is also annotation-driven.
 */
public class StudentServiceTest {
	
	/*
	 * We'll need an instance of the class we're testing!
	 * 
	 * We're now going to inject our mocks.
	 */
	@InjectMocks
	private static StudentService studentService; //object under test
	
	/*
	 * We do NOT need an instance of StudentRepository; we're NOT testing this.
	 * We are mocking it!
	 */
	@Mock
	private static StudentRepositoryImpl studentRepository; //a dependency of our object under test

	/*
	 * We usually want to do some setup for our test suites. For instance, maybe
	 * we want to create some dummy data to be used throughout our tests or open
	 * some resource that it is needed for the duration of our tests.
	 */
	
	@BeforeClass
	public static void setupBeforeClass() {
		/*
		 * This runs before an instance of our test class is created.
		 */
		
		studentService = new StudentService();
	}
	
	@Before
	public void setupBeforeEachMethod() {
		/*
		 * This runs once before every single @Test method.
		 * 
		 * Yes, Mockito is annotation-driven, but you still need to initialize the mocks
		 * in your setup.
		 */
		
		MockitoAnnotations.openMocks(this);
		
	}
	
	@Test
	@Ignore //allows us to NOT run this test
	public void sampleTest() {
		Assert.fail();
	}
	
	/*
	 * This is NOT a unit test; it's an integration test. This is because we
	 * also implicitly test the StudentRepository while running these tests.
	 */
//	@Test
//	public void testGetStudentByName() {
//		Student retrievedStudent = studentService.findByName("Christina");
//		//A test is NOT a test without an assertion. We need to check the result.
//		Assert.assertEquals("Christina", retrievedStudent.getName());
//		Student retrievedStudent2 = studentService.findByName("Roger");
//		Assert.assertNull(retrievedStudent2);
//	}
	
	@Test
	public void testGetStudentByName() {
		/*
		 * I'm going to use Mockito to control the output when the
		 * StudentRepositoryImpl class' findByName method is called.
		 * This is called stubbing - providing canned answers for
		 * method calls.
		 */
		
		Mockito.when(studentRepository.findByName("Roger")).thenReturn(
				new Student(1, "Roger", 33.4f, 1));
		
		Student retrievedStudent = studentService.findByName("Roger");
		Assert.assertEquals("Roger", retrievedStudent.getName());
	}
	
	@Test
	public void testFindAll() {
		
		Mockito.when(studentRepository.findAll()).thenReturn(
				Arrays.asList(
						new Student(1, "Roger", 234324.3f, 2),
						new Student(2, "Shelly", 343.4333f, 7),
						new Student(3, "John", 433.55f, 0))
				);
		
		List<Student> students = studentService.findAll();
		Assert.assertEquals("Shelly", students.get(1).getName());
	}
	
	@After
	public void teardownAfterEachMethod() {
		/*
		 * This runs once after every single @Test method.
		 */
	}
	
	@AfterClass
	public static void teardownAfterclass() {
		/*
		 * This runs once after all other methods in this class have been
		 * run. Now is the time to close any resources that have been opened.
		 */
	}
}
